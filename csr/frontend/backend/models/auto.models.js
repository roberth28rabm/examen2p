const mongoose = require('mongoose');
const {Schema}=mongoose;


const autoSchema = new Schema({
    modelo:{
        type:String,
        required: true,
    },
    marca:{
        type:String,
        required:true,
    },
    color:{
        type:String,
        required:true,
    },
    placa:{
        type:String,
        required:true,
    },
    pasajeros:{
        type:String,
        require:true,
    },
},{
    collection:'posts'
})


module.exports=mongoose.model('Auto', autoSchema)