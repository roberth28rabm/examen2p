let express = require('express'),
cors = require('cors'),
mongoose = require('mongoose'),
database = require('./database'),
bodyParser = require('body-parser');

//Conexion con base de dato
mongoose.Promise = global.Promise;
mongoose.connect(database.db,{
    useNewUrlParser:true,
    useUnifiedTopology:true
}).then(()=>{
    console.log("Conexion con base de datos");
},
error =>{
    console.log("No se pudo conectar con base de datos" +error);
})

const autoApi = require('../backend/routes/auto.route')
const app =express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:false
}))
app.use('/api', autoApi)
app.use(express.json())

const corsOptions ={
    origin:'http://localhost:4000', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
}
app.use(cors(corsOptions));


/*Crear puerto
const port = process.env.PORT || 4000;
const server = app.listen(port, ()=>{
    console.log('Conectado al puerto ' + port);
})*/

//error hanler
app.use(function(err,req,res,next){
    console.log(err.message);
    if(!err.statusCode)err.statusCode = 500;
    res.status(err.statusCode).send(err.message)
})
