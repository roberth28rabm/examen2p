const express = require('express');
const autoRoute = express.Router();

let AutoModel = require('../models/auto.models');

//Crear auto
autoRoute.route('/create-auto').post((req,res,next)=>{
    AutoModel.create(req.body, (error, data)=>{

        if(error){
            return next(error)
        } else{
            res.json(data)
        }
    })
})

module.exports = autoRoute;