const express = require("express");
//const { findById } = require("../models/cliente");
//const cliente = require("../models/cliente");
const router = express.Router();
const Cliente = require('../models/cliente');
const Auto = require('../models/auto');
const Propietario = require('../models/propietario');


//const multer = require('multer');

//Insertar cliente en base de datos
router.post('/add', (req, res)=>{
    const cliente =new Cliente ({ 
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        cedula: req.body.cedula,
        celular: req.body.celular,
        correo: req.body.correo,
        direccion: req.body.direccion,
        edad: req.body.edad,
        tipoLicencia: req.body.tipoLicencia

    });
    cliente.save((err)=>{
        if(err){
            res.json({message: err.message, type: 'danger'})
        }else{
            req.session.message = {
                type: 'success',
                message: 'Usuario agregado correctamente'
            };
            res.redirect("/");
        }
    })
} );


// get all clientes route
router.get("/", (req, res)=>{
    Cliente.find().exec((err,clientes)=>{
        if(err){
            res.json({ message: err.message});

        }else{
            res.render('index', {
                title: "Home Page",
                clientes: clientes,

            })
        }

    });
});

router.get("/add", (req, res)=>{
    res.render('add_cliente',{title:"Add Cliente"})
})
//Editar un cliente 
router.get("/edit/:id", (req, res) => {
    let id = req.params.id;
    Cliente.findById(id, (err, cliente) => {
        if (err) {
            res.redirect('/');
        } else {
            if (cliente == null) {
                res.redirect('/');
            } else {
                res.render("edit_cliente", {
                    title: "Cliente editado",
                    cliente: cliente
                })
            }
        }
    })

})

//actualizar clientes
router.post('/update/:id', (req, res)=>{
    let id= req.params.id;
    Cliente.findByIdAndUpdate(id,{
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        cedula: req.body.cedula,
        celular: req.body.celular,
        correo: req.body.correo,
        direccion: req.body.direccion,
        edad: req.body.edad,
        tipoLicencia: req.body.tipoLicencia

    },(err, result)=>{
        if(err){
            res.json({message: err.message, type: "danger"});
        }else{
            req.session.message={
                type: 'success',
                message: 'Cliente actualizado'
            };
            res.redirect("/");
        }
    })
    
})
//Delete client route
router.get('/delete/:id', (req, res)=>{
    let id = req.params.id;
    Cliente.findByIdAndRemove(id, (err)=>{
        if(err){
            res.json({message: err.message});
        }else{
            req.session.message={
                type: 'success',
                message: 'Cliente borrado'
            };
            res.redirect('/');
        }
    })
})

//PROPIETARIOOOO

router.post('/addPropietario', (req, res)=>{
    const propietario =new Propietario ({ 
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        cedula: req.body.cedula,
        celular: req.body.celular,
        correo: req.body.correo,
        direccion:req.body.direccion,
        edad:req.body.edad

    });
    propietario.save((err)=>{
        if(err){
            res.json({message: err.message, type: 'danger'})
        }else{
            req.session.message = {
                type: 'success',
                message: 'Propietario agregado correctamente'
            };
            res.redirect("/propietario");
        }
    })
} );


// get all Autos route
router.get("/propietario", (req, res)=>{
    Propietario.find().exec((err,propietarios)=>{
        if(err){
            res.json({ message: err.message});

        }else{
            res.render('indexPropietario', {
                title: "Home Page",
                propietarios: propietarios,

            })
        }

    });
});

router.get("/addPropietario", (req, res)=>{
    res.render('add_propietario',{title:"Add Propietario"})
})

//Editar un Auto 
router.get("/editPropietario/:id", (req, res) => {
    let id = req.params.id;
    Propietario.findById(id, (err, propietario) => {
        if (err) {
            res.redirect('/propietario');
        } else {
            if (propietario == null) {
                res.redirect('/propietario');
            } else {
                res.render("edit_propietario", {
                    title: "Propietario editado",
                    propietario: propietario
                })
            }
        }
    })

})

//actualizar clientes
router.post('/updatePropietario/:id', (req, res)=>{
    let id= req.params.id;
    Propietario.findByIdAndUpdate(id,{       
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        cedula: req.body.cedula,
        celular: req.body.celular,
        correo: req.body.correo,
        direccion:req.body.direccion,
        edad:req.body.edad

    },(err, result)=>{
        if(err){
            res.json({message: err.message, type: "danger"});
        }else{
            req.session.message={
                type: 'success',
                message: 'Propietario actualizado'
            };
            res.redirect("/propietario");
        }
    })
    
})
//Delete client route
router.get('/deletePropietario/:id', (req, res)=>{
    let id = req.params.id;
    Propietario.findByIdAndRemove(id, (err)=>{
        if(err){
            res.json({message: err.message});
        }else{
            req.session.message={
                type: 'success',
                message: 'Propitario borrado'
            };
            res.redirect('/propietario');
        }
    })
})

//AUTOOOOOOOOOOOOOOOOOOO
router.post('/addAuto', (req, res)=>{
    const auto =new Auto ({ 
        modelo: req.body.modelo,
        marca: req.body.marca,
        color: req.body.color,
        placa: req.body.placa,
        pasajeros: req.body.pasajeros

    });
    auto.save((err)=>{
        if(err){
            res.json({message: err.message, type: 'danger'})
        }else{
            req.session.message = {
                type: 'success',
                message: 'Auto agregado correctamente'
            };
            res.redirect("/auto");
        }
    })
} );


// get all Autos route
router.get("/auto", (req, res)=>{
    Auto.find().exec((err,autos)=>{
        if(err){
            res.json({ message: err.message});

        }else{
            res.render('indexAuto', {
                title: "Home Page",
                autos: autos,

            })
        }

    });
});

router.get("/addAuto", (req, res)=>{
    res.render('add_auto',{title:"Add Auto"})
})

//Editar un Auto 
router.get("/editAuto/:id", (req, res) => {
    let id = req.params.id;
    Auto.findById(id, (err, auto) => {
        if (err) {
            res.redirect('/auto');
        } else {
            if (auto == null) {
                res.redirect('/auto');
            } else {
                res.render("edit_auto", {
                    title: "Auto editado",
                    auto: auto
                })
            }
        }
    })

})

//actualizar clientes
router.post('/updateAuto/:id', (req, res)=>{
    let id= req.params.id;
    Auto.findByIdAndUpdate(id,{       
        modelo: req.body.modelo,
        marca: req.body.marca,
        color: req.body.color,            
        placa: req.body.placa,
        pasajeros: req.body.pasajeros

    },(err, result)=>{
        if(err){
            res.json({message: err.message, type: "danger"});
        }else{
            req.session.message={
                type: 'success',
                message: 'Auto actualizado'
            };
            res.redirect("/auto");
        }
    })
    
})
//Delete client route
router.get('/deleteAuto/:id', (req, res)=>{
    let id = req.params.id;
    Auto.findByIdAndRemove(id, (err)=>{
        if(err){
            res.json({message: err.message});
        }else{
            req.session.message={
                type: 'success',
                message: 'Auto borrado'
            };
            res.redirect('/auto');
        }
    })
})


module.exports = router;