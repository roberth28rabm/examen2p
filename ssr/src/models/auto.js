const mongoose = require('mongoose');
const {Schema, model}=mongoose;


const autoSchema = new Schema({
    modelo:{
        type:String,
        required: true,
    },
    marca:{
        type:String,
        required:true,
    },
    color:{
        type:String,
        required:true,
    },
    placa:{
        type:String,
        required:true,
    },
    pasajeros:{
        type:Number,
        require:true,
    }
})


module.exports=mongoose.model('Auto', autoSchema)