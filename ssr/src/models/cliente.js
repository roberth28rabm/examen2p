const mongoose = require('mongoose');
const {Schema, model}=mongoose

const clientSchema = new Schema({
    nombre:{
        type:String,
        required: true,
    },
    apellido:{
        type:String,
        required:true,
    },
    cedula:{
        type:Number,
        required:true,
    },
    celular:{
        type:Number,
        required:true,
    },
    correo:{
        type:String,
        require:true,
    },
    direccion:{
        type:String,
        required:true,
    },
    edad:{
        type:Number,
        require:true,
    },
    tipoLicencia:{
        type:String,
        require:true,
    }
})

module.exports=mongoose.model('Client', clientSchema)


