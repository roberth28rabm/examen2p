//imports
const express = require('express');
const mongoose = require('mongoose');
const session = require('express-session');
const morgan = require('morgan')

const app = express();
app.use(morgan('dev'));

// Conexión a Base de datos
const {APPLICATION_NAME, PORT, MONGO_URI } = require('./src/config')
mongoose.connect(MONGO_URI,
    {
        useNewUrlParser: true,
        useFindAndModify: true,
        useUnifiedTopology: true
    }
).then(() => {
    console.log('Conectado con la Base de datos');
}).catch((err) => console.log(err))

// milddlewares
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.use(session({
    secret: 'my secret key',
    saveUninitialized:true,
    resave: false

}))
app.use((req,res, next)=>{
    res.locals.message= req.session.message;
    delete req.session.message;

    next();
})

//set templates engine
app.set('views', './src/views');
app.set('view engine', 'ejs');



// route prefix
app.use("", require("./src/routes/routes"))

app.listen(PORT, () =>{
    console.log(`${APPLICATION_NAME} esta iniciando con el servidor http://localhost:${PORT}`);
});
